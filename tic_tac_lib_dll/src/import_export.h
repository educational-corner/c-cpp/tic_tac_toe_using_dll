#ifndef _IMPORT_EXPORT_H_
#define _IMPORT_EXPORT_H_

#ifdef TIC_TAC_LIBRARY_EXPORTS
#define TIC_TAC_LIBRARY __declspec(dllexport)
#else
#define TIC_TAC_LIBRARY __declspec(dllimport)
#endif

#endif //_IMPORT_EXPORT_H_