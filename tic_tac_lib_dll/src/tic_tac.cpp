#include "pch.h" // use stdafx.h in Visual Studio 2017 and earlier
#include <utility>
#include <limits.h>

#include "tic_tac.h"
#include <Windows.h>
#include <algorithm>
#include <thread>
#include <chrono>

TicTac::TicTac()
	:m_nSize( 3 )
{
	memset( m_matrix, 0, sizeof( m_matrix ) );

	refreshPositionsForRandom();

}

void TicTac::refreshPositionsForRandom()
{
	m_positionsForRandom.clear();

	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			m_positionsForRandom.push_back( Position( i, j ) );
			m_matrix[i][j] = 0;
		}
	}
}

bool TicTac::setFigure( int positionX, int positionY, int figure )
{
	if( m_matrix[positionX][positionY] != 0 )
	{
		return false;
	}
	else
	{
		m_matrix[positionX][positionY] = figure;

		m_positionsForRandom.erase( std::remove( m_positionsForRandom.begin(),
			m_positionsForRandom.end(),
			Position( positionX, positionY ) ),
			m_positionsForRandom.end() );

	}

	return true;
}

bool TicTac::setFigureToRandomPos( int& positionX, int& positionY, int figure )
{
	// No more 
	if( m_positionsForRandom.size() == 0 )
		return false;

	//std::this_thread::sleep_for( std::chrono::seconds( 1 ) );

/*============================================================================*/

	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			if( m_matrix[i][j] == 0 )
			{
				if( checkLines( i, j ) )
				{
					positionX = i;
					positionY = j;
					return setFigure( i, j, figure );
				}
			}
		}
	}

/*============================================================================*/

	int randomPos = rand() % m_positionsForRandom.size();

	positionX = m_positionsForRandom[randomPos].m_positionX;
	positionY = m_positionsForRandom[randomPos].m_positionY;

	return setFigure( m_positionsForRandom[randomPos].m_positionX,
					  m_positionsForRandom[randomPos].m_positionY,
					  figure );
}

bool TicTac::checkLines( int positionX, int positionY ) const
{
	if( checkHorisontalLine( positionX, positionY ) ||
		checkVerticalLine( positionX, positionY ) )
	{
		return true;
	}

	// If current position is in the middle - check both of diagonals
	if( ( positionX == positionY ) && ( positionX == 1 ) )
	{
		return ( checkUpDownDiagonal( positionX, positionY ) ||
			checkDownUpDiagonal( positionX, positionY ) );
	}

	// If current position is top-right or bottom-left - check DownUp diagonal
	if( ( positionX + positionY ) == 2 )
	{
		return checkDownUpDiagonal( positionX, positionY );
	}

	// If current position is top-left or bottom-right - check UpDown diagonal
	if( ( positionX == positionY ) )
	{
		return checkUpDownDiagonal( positionX, positionY );
	}

	return false;
}

bool TicTac::checkHorisontalLine( int positionX, int positionY ) const
{
	for( int i = 0; i < 2; i++ )
	{
		if( ( m_matrix[positionX][i] != m_matrix[positionX][i + 1] ) )
			return false;
	}
	return true;
}

bool TicTac::checkVerticalLine( int positionX, int positionY ) const
{
	for( int i = 0; i < 2; i++ )
	{
		if( ( m_matrix[i][positionY] != m_matrix[i + 1][positionY] ) )
			return false;
	}
	return true;
}

bool TicTac::checkUpDownDiagonal( int positionX, int positionY ) const
{
	for( int i = 0; i < 2; i++ )
	{
		if( m_matrix[i][i] != m_matrix[i + 1][i + 1] )
			return false;
	}
	return true;
}
bool TicTac::checkDownUpDiagonal( int positionX, int positionY ) const
{
	for( int i = 0; i < 2; i++ )
	{
		if( m_matrix[i][2 - i] != m_matrix[i + 1][2 - ( i + 1 )] )
			return false;
	}
	return true;
}

void TicTac::showMatrix() const
{
	for( int i = 0; i < m_nSize; i++ )
	{
		for( int j = 0; j < m_nSize; j++ )
		{
			std::cout << m_matrix[i][j] << ' ';
		}
		std::cout << std::endl;
	}
}

bool TicTac::isDraw() const
{
	return ( m_positionsForRandom.size() == 0 );
}

void TicTac::showMesBox( const wchar_t* message ) const
{
	MessageBox( NULL, message, L"Game just finished", MB_OK );
}