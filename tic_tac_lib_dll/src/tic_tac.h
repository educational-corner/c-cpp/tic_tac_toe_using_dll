#ifndef _TIC_TAC_H_
#define _TIC_TAC_H_

#include "import_export.h"

#include <iostream>
#include <vector>


class TIC_TAC_LIBRARY TicTac
{
public:

public:

	struct Position
	{
		Position( int x, int y )
			:m_positionX( x ), m_positionY( y )
		{
		};
		int m_positionX, m_positionY;
		bool operator == ( const Position& p1 )
		{
			return ( p1.m_positionX == m_positionX && p1.m_positionY == m_positionY );
		}
	};

public:

	TicTac();

	void showMatrix() const;

	bool setFigure( int positionX, int positionY, int figure );
	bool setFigureToRandomPos( int& positionX, int& positionY, int figure );

	bool checkLines( int positionX, int positionY ) const;
	bool isDraw() const;

	void refreshPositionsForRandom();
	void showMesBox( const wchar_t* message ) const;
private:

	bool checkHorisontalLine( int positionX, int positionY ) const;
	bool checkVerticalLine( int positionX, int positionY ) const;
	bool checkUpDownDiagonal( int positionX, int positionY ) const;
	bool checkDownUpDiagonal( int positionX, int positionY ) const;

private:

	std::vector<Position> m_positionsForRandom;



	int m_matrix[3][3];
	int m_nSize;
};

#endif //_TIC_TAC_H_