#include "../res/Resources.h"
#include "main_window.h"
#include <commctrl.h>
#pragma comment( lib, "comctl32.lib" )
#include <string>

LRESULT MainWindow::HandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch ( uMsg )
    {
    case WM_CREATE:
    {
        createIntroPage();
    }
    break;
    case WM_COMMAND:
    {
        if ( m_isSinglePlayerMode )
        {
            switchSinglePlayerMods( wParam );
        }

        switch ( wParam )
        {
        case SINGLE_PLAYER_BUTTON:
        {
            if( m_isSinglePlayerMode )
                break;

            deleteMultiPlayerWidgets();
            createSinglePlayerWidgets();
        }
        break;
        case MULTI_PLAYER_BUTTON:
        {
            if ( !m_isSinglePlayerMode )
                break;

            deleteSinglePlayerWidgets();
            createMultiPlayerWidgets();
        }
        break;
        case START_GAME_BUTTON:
        {
            
            if ( GetWindowText( m_usernameInputBar, m_userName1, 200 ) == 0 )
            {
                MessageBox( m_hwnd, L"Please enter username!", NULL, MB_OK );
                break;
            }
            if ( !m_isSinglePlayerMode )
            {
                if ( GetWindowText( m_secondNameInputBar, m_userName2, 200 ) == 0 )
                {
                    MessageBox( m_hwnd, L"Please enter second username!", NULL, MB_OK );
                    break;
                }
            }

            deleteIntroPage();

            createGamePage();

        }
        break;
        case FIELD_BUTTON:
        {
            if( m_gameIsFinished )
                break;
            if ( m_playersTurn )
            {
                if ( trySetGUIFigure( lParam, 1 ) )
                {
                    m_playersTurn = false;

                    if ( m_isSinglePlayerMode )
                    {
                        if ( computerSetGUIFigure( lParam, 2 ) )
                        {
                            m_playersTurn = true;
                        }
                    }
                }     
            }
            else
            {
                if ( !m_isSinglePlayerMode )
                {
                    if ( trySetGUIFigure( lParam, 2 ) )
                    {
                        m_playersTurn = true;
                    }
                }
            }
        }
        break;
        case TO_MENU_BUTTON:
        {
            deleteGamePage();
            refreshValues();
            createIntroPage();
        }
        break;
        case WM_CLOSE:
            DestroyWindow( m_hwnd );
            break;
        default:
            break;
        }
    }
    break;
    case WM_DESTROY:
        PostQuitMessage( 0 );
        return 0;

    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint( m_hwnd, &ps );
        FillRect( hdc, &ps.rcPaint, ( HBRUSH )( COLOR_WINDOW ) );
        EndPaint( m_hwnd, &ps );
    }
    return 0;

    default:
        return DefWindowProc( m_hwnd, uMsg, wParam, lParam );
    }
    return TRUE;
}

void MainWindow::createIntroPage()
{
    GetClientRect( m_hwnd, &m_clientRect );

    m_introText = CreateWindow( L"Static",
                                L"Welcome! Enter your username:",
                                WS_VISIBLE | WS_CHILD | ES_CENTER,
                                ( m_clientRect.right/3 ),
                                ( m_clientRect.bottom/15 ),
                                250, 30,
                                m_hwnd,
                                NULL, NULL, NULL );

    m_usernameInputBar = CreateWindow( L"Edit",
                                        L"Player 1",
                                        WS_VISIBLE | WS_CHILD | ES_CENTER | WS_BORDER,
                                        ( m_clientRect.right / 3 ),
                                        ( m_clientRect.bottom / 9 ),
                                        250, 30,
                                        m_hwnd,
                                        NULL, NULL, NULL );

    m_singlePlayerButton = CreateWindow( L"Button",
                                        L"Single Player",
                                        WS_VISIBLE | WS_CHILD | BS_FLAT | BS_PUSHBUTTON | WS_BORDER,
                                        ( m_clientRect.right / 3 ),
                                        ( m_clientRect.bottom / 5 ),
                                        100, 50,
                                        m_hwnd, ( HMENU )SINGLE_PLAYER_BUTTON, NULL, NULL );

    m_multiPlayerButton = CreateWindow( L"Button",
                                        L"Multi Player",
                                        WS_VISIBLE | WS_CHILD | BS_FLAT | BS_PUSHBUTTON | WS_BORDER,
                                        ( m_clientRect.right / 2 ) - 15,
                                        ( m_clientRect.bottom / 5 ),
                                        100, 50,
                                        m_hwnd,
                                        ( HMENU )MULTI_PLAYER_BUTTON, NULL, NULL );

    createSinglePlayerWidgets();

    m_playButton = CreateWindow( L"Button",
                                L"Play",
                                WS_VISIBLE | WS_CHILD | ES_CENTER,
                                ( m_clientRect.right / 3 ),
                                ( m_clientRect.bottom / 1.5 ),
                                250, 30,
                                m_hwnd,
                                ( HMENU )START_GAME_BUTTON, NULL, NULL );
}

void MainWindow::createSinglePlayerWidgets()
{
    m_isSinglePlayerMode = true;

    m_modeText = CreateWindow( L"Static",
                                L"Mode: Single Player",
                                WS_VISIBLE | WS_CHILD | ES_CENTER,
                                ( m_clientRect.right / 3 ),
                                ( m_clientRect.bottom / 3 ),
                                250, 30,
                                m_hwnd,
                                NULL, NULL, NULL );

    m_modeOptionText = CreateWindow( L"Static",
                                    L"Choose computer difficulty:",
                                    WS_VISIBLE | WS_CHILD | ES_CENTER,
                                    ( m_clientRect.right / 3 ),
                                    ( m_clientRect.bottom / 2.5 ),
                                    250, 30,
                                    m_hwnd,
                                    NULL, NULL, NULL );

    m_easyMode = CreateWindow( L"Button",
                                L"Easy Mode",
                                WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
                                ( m_clientRect.right / 3 ),
                                ( m_clientRect.bottom / 2.2 ),
                                200, 25,
                                m_hwnd,
                                ( HMENU )EASY_MODE, NULL, NULL );

    SendMessage( m_easyMode, BM_SETCHECK, 1, 0 );

    m_mediumMode = CreateWindow( L"Button",
                                L"Medium Mode",
                                WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
                                ( m_clientRect.right / 3 ),
                                ( m_clientRect.bottom / 2 ),
                                200, 25,
                                m_hwnd,
                                ( HMENU )MEDIUM_MODE, NULL, NULL );

    m_hardMode = CreateWindow( L"Button",
                                L"Hard Mode",
                                WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
                                ( m_clientRect.right / 3 ),
                                ( m_clientRect.bottom / 1.8 ),
                                200, 25,
                                m_hwnd,
                                ( HMENU )HARD_MODE, NULL, NULL );
}
void MainWindow::deleteSinglePlayerWidgets()
{
    DestroyWindow( m_modeText );
    DestroyWindow( m_modeOptionText );
    DestroyWindow( m_easyMode );
    DestroyWindow( m_mediumMode );
    DestroyWindow( m_hardMode );
}
void MainWindow::createMultiPlayerWidgets()
{
    m_isSinglePlayerMode = false;

    m_modeText = CreateWindow( L"Static",
                                L"Mode: Multi Player",
                                WS_VISIBLE | WS_CHILD | ES_CENTER,
                                ( m_clientRect.right / 3 ),
                                ( m_clientRect.bottom / 3 ),
                                250, 30,
                                m_hwnd,
                                NULL, NULL, NULL );

    m_modeOptionText = CreateWindow( L"Static",
                                    L"Choose other Players name:",
                                    WS_VISIBLE | WS_CHILD | ES_CENTER,
                                    ( m_clientRect.right / 3 ),
                                    ( m_clientRect.bottom / 2.5 ),
                                    250, 30,
                                    m_hwnd,
                                    NULL, NULL, NULL );

    m_secondNameInputBar = CreateWindow( L"Edit",
                                        L"Player 2",
                                        WS_VISIBLE | WS_CHILD | ES_CENTER | WS_BORDER,
                                        ( m_clientRect.right / 3 ),
                                        ( m_clientRect.bottom / 2.3 ),
                                        250, 30,
                                        m_hwnd,
                                        NULL, NULL, NULL );
}
void MainWindow::deleteMultiPlayerWidgets()
{
    DestroyWindow( m_modeText );
    DestroyWindow( m_modeOptionText );
    DestroyWindow( m_secondNameInputBar );
}

void MainWindow::deleteIntroPage()
{
    DestroyWindow( m_introText );
    DestroyWindow( m_usernameInputBar );
    DestroyWindow( m_singlePlayerButton );
    DestroyWindow( m_multiPlayerButton );
    DestroyWindow( m_playButton );

    if ( m_isSinglePlayerMode )
    {
        deleteSinglePlayerWidgets();
    }
    else
    {
        deleteMultiPlayerWidgets();
    }
}

void MainWindow::switchSinglePlayerMods( WPARAM& wParam )
{
    switch ( LOWORD( wParam ) )
    {
    case EASY_MODE:
    {
        SendMessage( m_easyMode, BM_SETCHECK, 1, 0 );
        SendMessage( m_mediumMode, BM_SETCHECK, 0, 0 );
        SendMessage( m_hardMode, BM_SETCHECK, 0, 0 );

        m_difficuty = MainWindow::EComputerDifficult::eCD_Easy;
    }
    break;
    case MEDIUM_MODE:
    {
        SendMessage( m_easyMode, BM_SETCHECK, 0, 0 );
        SendMessage( m_mediumMode, BM_SETCHECK, 1, 0 );
        SendMessage( m_hardMode, BM_SETCHECK, 0, 0 );

        m_difficuty = MainWindow::EComputerDifficult::eCD_Medium;

        MessageBox( m_hwnd, L"Not implemented Yet, easy mode on default", NULL, MB_OK );

        chooseDefaultEasyMode();
    }
    break;
    case HARD_MODE:
    {
        SendMessage( m_easyMode, BM_SETCHECK, 0, 0 );
        SendMessage( m_mediumMode, BM_SETCHECK, 0, 0 );
        SendMessage( m_hardMode, BM_SETCHECK, 1, 0 );

        m_difficuty = MainWindow::EComputerDifficult::eCD_Hard;

        MessageBox( m_hwnd, L"Not implemented Yet, easy mode on default", NULL, MB_OK );

        chooseDefaultEasyMode();
    }
    break;
    default:
        break;
    }
}

void MainWindow::chooseDefaultEasyMode()
{
    SendMessage( m_easyMode, BM_SETCHECK, 1, 0 );
    SendMessage( m_mediumMode, BM_SETCHECK, 0, 0 );
    SendMessage( m_hardMode, BM_SETCHECK, 0, 0 );

    m_difficuty = MainWindow::EComputerDifficult::eCD_Easy;
}

void MainWindow::createButtonNet()
{
    GetWindowRect( m_hwnd, &m_clientRect );

    for ( int i = 0; i < 3; i++ )
    {
        for ( int j = 0; j < 3; j++ )
        {
            m_buttonMatrix[i][j] = CreateWindow( L"Button",
                                                L"",
                                                WS_VISIBLE | WS_CHILD | BS_FLAT | BS_PUSHBUTTON | WS_BORDER,
                                                ( m_clientRect.right / 4 ) + ( j*120 ),
                                                ( m_clientRect.bottom / 11 ) + ( i*120 ),
                                                100, 100,
                                                m_hwnd, ( HMENU )FIELD_BUTTON, NULL, NULL );
        }
    }
}

void MainWindow::deleteButtonNet()
{
    for ( int i = 0; i < 3; i++ )
    {
        for ( int j = 0; j < 3; j++ )
        {
            DestroyWindow( m_buttonMatrix[i][j] );
        }
    }
}

bool MainWindow::trySetGUIFigure( LPARAM& lParam, int figure )
{
    for ( int i = 0; i < 3; i++ )
    {
        for ( int j = 0; j < 3; j++ )
        {
            if ( ( HWND )lParam == m_buttonMatrix[i][j] )
            {
                if ( m_ticTac.setFigure( i, j, figure ) )
                {
                    SendMessage( m_buttonMatrix[i][j],
                                WM_SETTEXT,
                                0,
                                ( ( figure == 1 ) ? ( LPARAM )( L"X" ) : ( LPARAM )( L"O" ) ) );

                    if( !m_isSinglePlayerMode )
                        ( ( figure == 1 ) ? sentInfoMessage( 0, L"Turn: " ) : sentInfoMessage( 1, L"Turn: " ) );

                    if ( m_ticTac.checkLines( i, j ) )
                    {
                        sentInfoMessage( figure, L"Winner: " );

                        showWinnerForMulti( figure );

                        m_gameIsFinished = true;
                        return false;
                    }
                    if ( m_ticTac.isDraw() )
                    {
                        m_ticTac.showMesBox( L"It's a Draw!" );

                        m_gameIsFinished = true;
                        return false;
                    }

                    return true;
                }
                // If you click on a full field - nothing happens
                else
                {
                    return false;
                }
            }
        }
    }
}

bool MainWindow::computerSetGUIFigure( LPARAM& lParam, int figure )
{
    switch ( m_difficuty )
    {
    case EComputerDifficult::eCD_Easy:
    {
        int xPos = 0;
        int yPos = 0;

        if ( m_ticTac.setFigureToRandomPos( xPos, yPos, figure ) )
        {
            SendMessage( m_buttonMatrix[xPos][yPos],
                        WM_SETTEXT,
                        0,
                        ( ( figure == 1 ) ? ( LPARAM )( L"X" ) : ( LPARAM )( L"O" ) ) );
        }

        if ( m_ticTac.checkLines( xPos, yPos ) )
        {
            m_ticTac.showMesBox( L"Computer Won!" );

            m_gameIsFinished = true;
            return false;
        }
        if ( m_ticTac.isDraw() )
        {
            m_ticTac.showMesBox( L"It's a Draw!" );

            m_gameIsFinished = true;
            return false;
        }

        return true;
    }
    break;
    case EComputerDifficult::eCD_Medium:
    {

    }
    break;
    case EComputerDifficult::eCD_Hard:
    {

    }
    break;
    default:
        break;
    }
}

void MainWindow::createGamePage()
{
    createButtonNet();

    std::wstring message = L"Turn: ";
    message += m_userName1;

    m_infoText = CreateWindow(  L"Static",
                                message.c_str(),
                                WS_VISIBLE | WS_CHILD | ES_CENTER,
                                ( m_clientRect.right / 1.7 ),
                                ( m_clientRect.bottom / 10 ),
                                250, 30,
                                m_hwnd,
                                NULL, NULL, NULL );

    m_toMenuButton = CreateWindow(  L"Button",
                                    L"To Menu",
                                    WS_VISIBLE | WS_CHILD | BS_FLAT | BS_PUSHBUTTON | WS_BORDER,
                                    ( m_clientRect.right / 1.6 ),
                                    ( m_clientRect.bottom / 6 ),
                                    100, 50,
                                    m_hwnd,
                                    ( HMENU )TO_MENU_BUTTON, NULL, NULL );

    m_exitButton = CreateWindow( L"Button",
                                L"Exit",
                                WS_VISIBLE | WS_CHILD | BS_FLAT | BS_PUSHBUTTON | WS_BORDER,
                                ( m_clientRect.right / 1.6 ) + 120,
                                ( m_clientRect.bottom / 6 ),
                                100, 50,
                                m_hwnd,
                                ( HMENU )WM_CLOSE, NULL, NULL );
}
void MainWindow::deleteGamePage()
{
    deleteButtonNet();
    DestroyWindow( m_infoText );
    DestroyWindow( m_toMenuButton );
    DestroyWindow( m_exitButton );
}

void MainWindow::sentInfoMessage( int figure, const wchar_t* mess )
{
    std::wstring message;
    message = mess;
    ( figure == 1 ) ? ( message += m_userName1 ) : ( message += m_userName2 );

    SendMessage( m_infoText,
                WM_SETTEXT,
                0,
                ( LPARAM )message.c_str() );
}

void MainWindow::refreshValues()
{
    m_ticTac.refreshPositionsForRandom();
    m_gameIsFinished = false;
    m_isSinglePlayerMode = true;
    m_playersTurn = true;
}

void MainWindow::showWinnerForMulti( int figure )
{
    std::wstring message;
    message = L"Winner is: ";
    ( figure == 1 ) ? ( message += m_userName1 ) : ( message += m_userName2 );
    m_ticTac.showMesBox( message.c_str() );
}