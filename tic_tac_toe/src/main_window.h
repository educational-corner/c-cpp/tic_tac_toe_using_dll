#ifndef _MAIN_WINDOW_H_
#define _MAIN_WINDOW_H_

#include "base_win.h"
#include "tic_tac.h"
#include <richedit.h>
#include <string>

class MainWindow : public BaseWindow<MainWindow>
{
public:

    enum EComputerDifficult
    {
        eCD_Easy,
        eCD_Medium,
        eCD_Hard
    };

public:

    PCWSTR  ClassName() const { return L"Sample Window Class"; }
    LRESULT HandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam );
    
private:

    bool m_isSinglePlayerMode = true;
    void createIntroPage();
    void deleteIntroPage();

    void createSinglePlayerWidgets();
    void deleteSinglePlayerWidgets();
    void createMultiPlayerWidgets();
    void deleteMultiPlayerWidgets();

    void createGamePage();
    void deleteGamePage();
    void createButtonNet();
    void deleteButtonNet();

    void sentInfoMessage(int figure, const wchar_t *mess);
    void showWinnerForMulti( int figure );

    void refreshValues();

private:

    RECT m_clientRect;

    HWND m_introText;
    HWND m_infoText;
    HWND m_usernameInputBar;
    HWND m_secondNameInputBar;
    HWND m_singlePlayerButton;
    HWND m_multiPlayerButton;

    HWND m_modeText;
    HWND m_modeOptionText;

    HWND m_playButton;

    HWND m_toMenuButton;
    HWND m_exitButton;

private:

    HWND m_easyMode;
    HWND m_mediumMode;
    HWND m_hardMode;

    EComputerDifficult m_difficuty;

    void switchSinglePlayerMods( WPARAM& wParam );

    void chooseDefaultEasyMode();

private:

    HWND m_buttonMatrix[3][3];

private:

    TicTac m_ticTac;

    bool m_playersTurn = true;
    bool m_gameIsFinished = false;

    bool trySetGUIFigure( LPARAM& lParam, int figure );
    bool computerSetGUIFigure( LPARAM& lParam, int figure );

private:

    wchar_t m_userName1[200];
    wchar_t m_userName2[200];
};


#endif //_MAIN_WINDOW_H_
